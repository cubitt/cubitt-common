export declare class Guid {
    private guid;
    constructor(g: string);
    ToString(): string;
    static parse(guid: string): Guid;
    static newGuid(): Guid;
}
