# Cubitt Common
Open this project in [`atom-typescript`](https://atom.io/packages/atom-typescript). Press F6 to build.

This project provides common base elements that are used across the entire cubitt stack.


## How to generate documentation
Run the following commands:
> npm install --dev

Then run:
> node_modules/.bin/typedoc --out doc/ --module commonjs --target ES5 --mode file  src/

Now you can view the documentation by opening the doc/index.html file using your browser.
