/**
 * Interface that represents a simple Dictionary
 */
export interface Dictionary<T> {
    [K: string]: T;
}
